package exceptions1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Calculator {
	static double vich;
	static double sum;
	static double del;
	static double ymn;
	
	public static double summa() {
	    double summa = 0;
		try {
			 summa = operationSum();
		} catch (InputMismatchException e) {
			System.out.println("������ ������� �����");
		}
		  catch (IllegalArgumentException e) {
			System.out.println("��������� �� ����� ���� ��������������");
		}  
		return summa;
	    }
	
	public static double difference() {
		double difference = 0;
		try {
			difference = operationVich();
		} catch (InputMismatchException e) {
			System.out.println("������ ������� �����");
		}
		  catch (IllegalArgumentException e) {
			System.out.println("��������� �� ����� ���� ��������������");
		}
		return difference;
	}
	
	public static double composition() {
		double composition = 0;
		try {
			composition = operationYmn();
		} catch (InputMismatchException e) {
			System.out.println("������ ������� �����");
		}
		  catch (IllegalArgumentException e) {
			System.out.println("��������� �� ����� ���� ������ ����");
		}
		return composition;
	}
	
	public static double quotient() {
		double quotient = 0;
		try {
			quotient = operationDel();
		} catch (ArithmeticException e) {
			System.out.println("������ ������ �� ����");
		}
		  catch (InputMismatchException e) {
			System.out.println("������ ������� �����");
		}
		  catch (IllegalArgumentException e) {
			System.out.println("��������� �� ����� ���� ��������������");
		}
		return quotient;
	}
	
	
	public static double operationSum() {
		System.out.print("������� ������ ��������� ��� �����: ");
		Scanner scan = new Scanner(System.in);
		double a = scan.nextDouble();
		System.out.print("������� ������ ��������� ��� �����: ");
		Scanner scan1 = new Scanner(System.in);
		double b = scan1.nextDouble();
		
		if (a<0 || b<0)
			throw new IllegalArgumentException("��������� �� ����� ���� ������ ����");
		sum = a+b;
		return sum;
	}	
	
	public static double operationVich() {
		System.out.print("������� ������ ��������� ��� ��������: ");
		Scanner scan = new Scanner(System.in);
		double a = scan.nextDouble();
		System.out.print("������� ������ ��������� ��� ��������: ");
		Scanner scan1 = new Scanner(System.in);
		double b = scan1.nextDouble();
		if (a < 0 || b < 0)
			throw new IllegalArgumentException("��������� �� ����� ���� ������ ����");
		vich = a - b;
		return vich;
	}
	
	public static double operationYmn() {
		System.out.print("������� ������ ��������� ��� ���������: ");
		Scanner scan = new Scanner(System.in);
		double a = scan.nextDouble();
		System.out.print("������� ������ ��������� ��� ���������: ");
		Scanner scan1 = new Scanner(System.in);
		double b = scan1.nextDouble();
		if (a < 0 || b < 0)
			throw new IllegalArgumentException("��������� �� ����� ���� ��������������");
		ymn = a * b;
		return ymn;
	}
	
	public static double operationDel() {
		System.out.print("������� ������ ��������� ��� �������: ");
		Scanner scan = new Scanner(System.in);
		double a = scan.nextDouble();
		System.out.print("������� ������ ��������� ��� �������: ");
		Scanner scan1 = new Scanner(System.in);
		double b = scan1.nextDouble();
		if (a == 0 || b == 0) 
			throw new ArithmeticException("������ ����������� ������� �� ����");
		del = a / b;
		return del;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}